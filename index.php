<!doctype html>
<html class="no-js" lang="id">

<head>
    <meta charset="utf-8">
    <title>Aplikasi Reminder Otomatis via WhatsApp dan Email kepada Pasien Klinik</title>
    <meta name="description"
          content="Aplikasi Reminder Otomatis ke Pasien menggunakan Whatsapp dan Email untuk Operasional Klinik">
    <meta name="keywords"
          content="aplikasi reminder whatsapp, aplikasi whatsapp pasien, aplikasi email pasien, reminder otomatis pasien, reminder whatsapp pasien, reminder whatsapp otomatis pasien">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" href="assets/images/favicon.png" type="image/png">
    <link rel="stylesheet" href="assets/css/plugins/hubpasien.min.css">

</head>

<body>
<header class="header-area">
    <nav class="navbar navbar-expand-lg">
        <div class="container position-relative">
            <a class="navbar-brand" href="">
                <img src="assets/images/hub-pasien-logo.png" alt="Logo Hub Pasien">
            </a>

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarContent">
                <span class="toggler-icon"></span>
                <span class="toggler-icon"></span>
                <span class="toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse sub-menu-bar" id="navbarContent">
                <ul class="navbar-nav ml-auto">
                    <li class="active"><a class="page-scroll" href="#home">Beranda</a></li>
                    <li><a class="page-scroll" href="#features">Fitur</a></li>
                    <li><a class="page-scroll" href="#pricing">Harga</a></li>
                    <li><a class="page-scroll" href="#screenshot">Galeri Aplikasi</a></li>
                    <li><a class="page-scroll" href="#footer">Kontak Kami</a></li>
                </ul>
                <div class="navbar-btn">
                    <a href="https://wa.link/ht4e8a" target="_blank" class="main-btn"><i class="fas fa-check"></i>
                        Registrasi</a>
                </div>
            </div>
        </div>
    </nav>
</header>

<section id="home" class="banner-area">
    <div class="banner-shape-1">
        <img src="assets/images/shape/shape-2.png" alt="Shape">
    </div>
    <div class="banner-shape-2">
        <img src="assets/images/shape/shape-1.png" alt="Shape">
    </div>
    <div class="banner-shape-3">
        <img src="assets/images/shape/dots.png" alt="Shape dots">
    </div>

    <div class="banner-content-wrapper">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6">
                    <div class="banner-content">
                        <h1 class="title wow fadeInUp" data-wow-duration="1.3s" data-wow-delay="0.2s">
                            Solusi Reminder Pasien via WhatsApp<br/>&<br/>Permudah Operasional Klinik
                        </h1>
                        <p class="wow fadeInUp" data-wow-duration="1.3s" data-wow-delay="0.6s">
                            Mudahkan komunikasi menggunakan Reminder Otomatis dengan Pasien via WhatsApp,
                            sehingga Potensi Calon Pasien baru semakin Besar.
                        </p>
                        <p class="wow fadeInUp" data-wow-duration="1.3s" data-wow-delay="0.6s">
                            Mudahkan operasional klinik
                            untuk memaksimalkan kinerja dengan Sistem di HubPasien.com
                        </p>

                        <ul class="download-btn">
                            <li>
                                <a href="https://wa.link/ht4e8a" target="_blank" class="google-play wow fadeInUp"
                                   data-wow-duration="1.3s" data-wow-delay="0.9s">
                                    <i class="fas fa-check"></i>
                                    <span class="text-1">Klik disini untuk</span>
                                    <span class="text-2">Register Aplikasi</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="banner-image wow fadeInRightBig" data-wow-duration="1.3s" data-wow-delay="0.8s">
                        <div class="image">
                            <img src="assets/images/ilustrasi-klinik.png" alt="Ilustrasi Aplikasi Reminder Otomatis via WhatsApp ke Pasien di Klinik" title="Ilustrasi Aplikasi Reminder Otomatis via WhatsApp ke Pasien di Klinik">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="features" class="features-area">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="section-title text-center">
                    <p class="sub-title">Menyediakan Fitur Aplikasi Klinik Terbaik</p>
                    <h2 class="title">Daftar Fitur Aplikasi</h2>
                </div>
            </div>
        </div>
        <div class="features-wrapper">
            <div class="row">
                <div class="col-lg-4">
                    <div class="single-features features-1 wow fadeInUpBig" data-wow-duration="1s"
                         data-wow-delay="0.8s">
                        <span class="features-number">01</span>
                        <div class="features-icon">
                            <i class="flaticon-smartphone-1"></i>
                        </div>
                        <div class="features-content">
                            <h3 class="features-title"><a href="https://wa.link/ht4e8a" target="_blank">Reminder
                                    Otomatis Pasien dengan <u>WhatsApp</u></a>
                            </h3>
                            <p>Otomatiskan Reminder Jadwal Konsultasi, Operasi, Tindakan dan Kontrol via
                                WhatsApp</p>
                        </div>
                        <div class="features-btn">
                            <a href="https://wa.link/ht4e8a" target="_blank"><i class="fal fa-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="single-features features-1 wow fadeInUpBig" data-wow-duration="1s"
                         data-wow-delay="0.5s">
                        <span class="features-number">02</span>
                        <div class="features-icon">
                            <i class="flaticon-smartphone"></i>
                        </div>
                        <div class="features-content">
                            <h3 class="features-title"><a href="https://wa.link/ht4e8a" target="_blank">Reminder Jadwal
                                    Operasional Klinik via Email</a></h3>
                            <p>Reminder jadwal operasional klinik untuk hari selanjutnya via Email</p>
                        </div>
                        <div class="features-btn">
                            <a href="https://wa.link/ht4e8a" target="_blank"><i class="fal fa-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="single-features features-1 wow fadeInUpBig" data-wow-duration="1s"
                         data-wow-delay="0.2s">
                        <span class="features-number">03</span>
                        <div class="features-icon">
                            <i class="flaticon-smartphone-2"></i>
                        </div>
                        <div class="features-content">
                            <h3 class="features-title"><a href="https://wa.link/ht4e8a" target="_blank">Permudah
                                    Merangkum Jadwal Operasional</a></h3>
                            <p>Permudah Operasional Klinik dengan Tampilan Rangkuman Tasks dalam Kalender</p>
                        </div>
                        <div class="features-btn">
                            <a href="https://wa.link/ht4e8a" target="_blank"><i class="fal fa-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="single-features features-1 wow fadeInUpBig" data-wow-duration="1s"
                         data-wow-delay="0.2s">
                        <span class="features-number">04</span>
                        <div class="features-icon">
                            <i class="flaticon-smartphone-2"></i>
                        </div>
                        <div class="features-content">
                            <h3 class="features-title"><a href="https://wa.link/ht4e8a" target="_blank">Solusi Permudah
                                    Sistem Operasional Klinik</a></h3>
                            <p>Efisiensikan waktu operasional klinik dengan sistem komputer secara
                                Online</p>
                        </div>
                        <div class="features-btn">
                            <a href="https://wa.link/ht4e8a" target="_blank"><i class="fal fa-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="single-features features-1 wow fadeInUpBig" data-wow-duration="1s"
                         data-wow-delay="0.5s">
                        <span class="features-number">05</span>
                        <div class="features-icon">
                            <i class="flaticon-smartphone"></i>
                        </div>
                        <div class="features-content">
                            <h3 class="features-title"><a href="https://wa.link/ht4e8a" target="_blank">Aplikasi
                                    Komputer Cetak Rekam Medis & Kwitansi</a>
                            </h3>
                            <p>Permudah proses cetak rekam medis & kwitansi pembayaran Pasien</p>
                        </div>
                        <div class="features-btn">
                            <a href="https://wa.link/ht4e8a" target="_blank"><i class="fal fa-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="single-features features-1 wow fadeInUpBig" data-wow-duration="1s"
                         data-wow-delay="0.8s">
                        <span class="features-number">06</span>
                        <div class="features-icon">
                            <i class="flaticon-smartphone-1"></i>
                        </div>
                        <div class="features-content">
                            <h3 class="features-title"><a href="https://wa.link/ht4e8a" target="_blank">Privasi Data
                                    Klinik Dijamin Keamanannya</a></h3>
                            <p>Jaminan keamanan data karena menggunakan Backup Otomatis</p>
                        </div>
                        <div class="features-btn">
                            <a href="https://wa.link/ht4e8a" target="_blank"><i class="fal fa-arrow-right"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="everything" class="everything-area">
    <div class="everything-shape shape-1"></div>
    <div class="everything-shape shape-2"></div>

    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="everything-image mt-50 wow fadeInLeftBig" data-wow-duration="1s" data-wow-delay="0.3s">
                    <div class="image">
                        <img src="assets/images/aplikasi-reminder-otomatis-via-whatsapp-desain-responsive.png" alt="aplikasi Reminder Otomatis via WhatsApp dengan Desain Responsive">
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="everything-content-wrapper mt-50">
                    <div class="section-title">
                        <p class="sub-title">Menyediakan Fitur Aplikasi Klinik Terbaik</p>
                        <h2 class="title">Mudah Diakses melalui Smartphone, Laptop, Komputer maupun Tablet</h2>
                    </div>
                    <div class="everything-content">
                        <ul class="everything-items">
                            <li>
                                <span>01</span>
                                <p>Permudah Akses Proses Operasional Klinik Anda melalui Genggaman Smartphone secara
                                    Online</p>
                            </li>
                            <li>
                                <span>02</span>
                                <p>Menjaga Hubungan Klinik dengan Pasien menggunakan fitur Reminder via
                                    WhatsApp</p>
                            </li>
                        </ul>
                        <a href="#" class="main-btn main-btn-2"><i class="fas fa-check"></i> Klik Untuk Registrasi
                            Aplikasi</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="powerful" class="powerful-tools-area">
    <div class="powerful-shape shape-1"></div>
    <div class="powerful-shape shape-2">
        <img src="assets/images/shape/shape-5.png" alt="Shape 5">
    </div>

    <div class="container">
        <div class="powerful-tools-wrapper">
            <div class="row">
                <div class="col-lg-6">
                    <div class="powerful-image mt-50 wow fadeInLeftBig" data-wow-duration="1s" data-wow-delay="0.3s">
                        <div class="image">
                            <img src="assets/images/jadwal-kalender-operasional-reminder-klinik.png" alt="Jadwal Kalender Operasional Reminder Klinik">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="powerful-tools-content mt-50">
                        <div class="section-title">
                            <p class="sub-title">Menyediakan Fitur Aplikasi Klinik Terbaik</p>
                            <h2 class="title">Permudah Reminder Pasien & Operasional Klinik</h2>
                        </div>
                        <div class="powerful-content-wrapper">
                            <ul class="content-list">
                                <li><i class="fas fa-check"></i> Reminder Pasien secara Otomatis dengan WhatsApp.</li>
                                <li><i class="fas fa-check"></i> Rangkum Jadwal Operasional pada Kalender Task.</li>
                                <li><i class="fas fa-check"></i> Manajemen Data Pasien secara Terstruktur & Online.</li>
                                <li><i class="fas fa-check"></i> Skema Digital Operasional Pasien dengan Mudah.</li>
                            </ul>
                            <a href="https://wa.link/ht4e8a" target="_blank" class="main-btn main-btn-2"><i
                                        class="fas fa-check"></i> Klik Untuk Registrasi
                                Aplikasi</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="testimonial" class="testimonial-area bg_cover"
         style="background-image: url(assets/images/testimonial-bg.jpg);">
    <div class="testimonial-shape shape-1"></div>
    <div class="testimonial-shape shape-2"></div>

    <div class="testimonial-wrapper">
        <div class="author-2">
            <img src="assets/images/dokter-ari.png" alt="Dokter Ari">
        </div>
        <div class="author-4">
            <img src="assets/images/author-4.jpg" alt="Author 4">
        </div>

        <div class="author-5">
            <img src="assets/images/author-5.jpg" alt="Author 5">
        </div>
        <div class="author-7">
            <img src="assets/images/author-7.jpg" alt="Author 7">
        </div>
    </div>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="section-title text-center">
                    <p class="sub-title">Tanggapan Pengguna HubPasien.com</p>
                    <h2 class="title">Testimonial Pengguna</h2>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-xl-8 col-lg-10">
                <div class="testimonial-bg">
                    <div class="testimonial-active swiper-container">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide single-testimonial text-center">
                                <p>Sekarang menjadi praktis pekerjaan Kami di klinik, yang awalnya menajemen data dan
                                    CRM klien manual sekarang menjadi otomatis. Mantappp</p>
                                <h4 class="author-name">Komang Adiaksa</h4>
                                <h5 style="color: white">- Rumah Sunat Bali</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="pricing" class="pricing-area">
    <div class="pricing-shape shape-1">
        <img src="assets/images/shape/dots-3.png" alt="shapre dots">
    </div>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="section-title text-center">
                    <p class="sub-title">Menyediakan Fitur Aplikasi Klinik Terbaik</p>
                    <h2 class="title">Solusi Harga Terbaik</h2>
                </div>
            </div>
        </div>
        <div class="pricing-wrapper">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                    <div class="single-pricing pricing-1 text-center wow fadeInLeftBig" data-wow-duration="1s"
                         data-wow-delay="0.4s">
                        <div class="pricing-icon">
                            <i class="flaticon-send"></i>
                        </div>
                        <div class="pricing-price">
                            <h5 class="sub-title">Paket WhatsApp</h5>
                            <span class="price">399.000</span>
                            <span class="price-sub">/ bulan</span>
                        </div>
                        <div class="pricing-body">
                            <ul class="pricing-list">
                                <li><i class="fas fa-check"></i> Reminder Otomatis via WhatsApp & Email</li>
                                <li><i class="fas fa-check"></i> Manajemen Reminder</li>
                                <li><i class="fas fa-check"></i> Kalender Reminder Klinik</li>
                                <li class="not-include"><i class="fas fa-times"></i> Manajemen Konsultasi</li>
                                <li class="not-include"><i class="fas fa-times"></i> Manajemen Tindakan</li>
                                <li class="not-include"><i class="fas fa-times"></i> Manajemen Kontrol</li>
                                <li class="not-include"><i class="fas fa-times"></i> Manajemen Follow Up</li>
                                <li class="not-include"><i class="fas fa-times"></i> Manajemen Data Rekam Medis</li>
                                <li class="not-include"><i class="fas fa-times"></i> POS Klinik</li>
                            </ul>
                            <div class="text-center">
                                <a href="#" class="main-btn">Pilih Paket</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                    <div class="single-pricing pricing-2 text-center wow fadeInUpBig" data-wow-duration="1s"
                         data-wow-delay="0.4s">
                        <div class="pricing-icon">
                            <i class="flaticon-plane"></i>
                        </div>
                        <div class="pricing-price">
                            <h5 class="sub-title">Paket Operasional</h5>
                            <span class="price">599.000</span>
                            <span class="price-sub">/ bulan</span>
                        </div>
                        <div class="pricing-body">
                            <ul class="pricing-list">
                                <li><i class="fas fa-check"></i> Reminder Otomatis via WhatsApp & Email</li>
                                <li><i class="fas fa-check"></i> Manajemen Reminder</li>
                                <li><i class="fas fa-check"></i> Kalender Reminder Klinik</li>
                                <li><i class="fas fa-check"></i> Manajemen Konsultasi</li>
                                <li><i class="fas fa-check"></i> Manajemen Tindakan</li>
                                <li><i class="fas fa-check"></i> Manajemen Kontrol</li>
                                <li><i class="fas fa-check"></i> Manajemen Follow Up</li>
                                <li class="not-include"><i class="fas fa-times"></i> Manajemen Data Rekam Medis</li>
                                <li class="not-include"><i class="fas fa-times"></i> POS Klinik</li>
                            </ul>
                            <div class="text-center">
                                <a href="https://wa.link/ht4e8a" target="_blank" class="main-btn main-btn-2">Pilih
                                    Paket</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
                    <div class="single-pricing pricing-3 text-center wow fadeInRightBig" data-wow-duration="1s"
                         data-wow-delay="0.4s">
                        <div class="pricing-icon">
                            <i class="flaticon-shuttle"></i>
                        </div>
                        <div class="pricing-price">
                            <h5 class="sub-title">Paket Lengkap</h5>
                            <span class="price">799.000</span>
                            <span class="price-sub">/ bulan</span>
                        </div>
                        <div class="pricing-body">
                            <ul class="pricing-list">
                                <li><i class="fas fa-check"></i> Reminder Otomatis via WhatsApp & Email</li>
                                <li><i class="fas fa-check"></i> Manajemen Reminder</li>
                                <li><i class="fas fa-check"></i> Kalender Reminder Klinik</li>
                                <li><i class="fas fa-check"></i> Manajemen Konsultasi</li>
                                <li><i class="fas fa-check"></i> Manajemen Tindakan</li>
                                <li><i class="fas fa-check"></i> Manajemen Kontrol</li>
                                <li><i class="fas fa-check"></i> Manajemen Follow Up</li>
                                <li><i class="fas fa-check"></i> Manajemen Data Rekam Medis</li>
                                <li><i class="fas fa-check"></i> POS Klinik</li>
                            </ul>

                            <div class="text-center">
                                <a href="https://wa.link/ht4e8a" target="_blank" class="main-btn">Pilih Paket</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="screenshot" class="screenshot-area">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="section-title text-center">
                    <p class="sub-title">Tampilan Aplikasi</p>
                    <h2 class="title">Daftar Tampilan Aplikasi Reminder Otomatis via WhatsApp</h2>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid custom-container">
        <div class="screenshot-active swiper-container">
            <div class="swiper-wrapper">
                <?php $list = scandir('assets/images/screens/list/', 1);
                foreach ($list as $row) {
                    if (!in_array($row, array('.', '..'))) {
                        $title = ucwords(str_replace(array('-','.png'), array(' ',''), $row));
                        ?>
                        <div class="swiper-slide single-screenshot">
                            <img src="assets/images/screens/list/<?php echo $row ?>" alt="<?php echo $title ?>" title="<?php echo $title ?>">
                        </div>
                        <?php
                    }
                } ?>
            </div>
        </div>
        <div class="screenshot-pagination"></div>
    </div>
</section>

<section id="brand" class="brand-download-area">
    <div class="container">

        <div class="download-area">
            <div class="row">
                <div class="col-lg-8">
                    <div class="download-content">
                        <h2 class="title">Segera Optimalkan Klinik Anda</h2>
                        <p>dengan klik tombol dibawah untuk melakukan registrasi dan segera dapatkan akun
                            Aplikasinya</p>
                        <ul class="download-btn">
                            <li>
                                <a class="google-play" href="https://wa.link/ht4e8a" target="_blank">
                                    <i class="fas fa-check"></i>
                                    <span class="text-1">Klik Disini untuk</span>
                                    <span class="text-2">Register Aplikasi</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="download-image wow fadeInUpBig" data-wow-duration="1s" data-wow-delay="0.4s">
                        <img src="assets/images/download-app.png" alt="Download Apps">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<footer id="footer" class="footer-area bg_cover" style="background-image: url(assets/images/footer-bg.jpg);">
    <div class="footer-shape shape-1"></div>

    <div class="container">
        <div class="footer-widget">
            <div class="row">
                <div class="col-lg-12">
                    <div class="footer-widget-wrapper">
                        <div class="footer-about mt-45">
                            <h4 class="footer-title">Tentang Kami</h4>
                            <p>Kami memberikan Pelayanan & Kenyamanan Maksimal bagi Pengguna HubPasien.com</p>
                            <ul class="social">
                                <li><a href="https://www.facebook.com/hubpasien" target="_blank"><i
                                                class="fab fa-facebook-f"></i></a></li>
                                <li><a href="https://www.instagram.com/hubpasien" target="_blank"><i
                                                class="fab fa-instagram"></i></a></li>
                            </ul>
                        </div>

                        <div class="footer-link mt-45">
                            <h4 class="footer-title">Daftar Menu</h4>

                            <ul class="link">
                                <li><a href="#">Beranda</a></li>
                                <li><a href="#">Fitur</a></li>
                                <li><a href="#">Harga</a></li>
                                <li><a href="#">Galeri Aplikasi</a></li>
                                <li><a href="#">Kontak Kami</a></li>
                            </ul>
                        </div>

                        <div class="footer-contact mt-45">
                            <h4 class="footer-title">Kontak Kami</h4>

                            <ul class="contact-info">
                                <li>
                                    <div class="single-contact">
                                        <i class="fas fa-phone-square"></i>
                                        <div class="contact-text">
                                            <p><a href="https://wa.link/ht4e8a" target="_blank">0819 3436 4063</a></p>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="single-contact">
                                        <i class="fas fa-envelope"></i>
                                        <div class="contact-text">
                                            <p><a href="mailto:hubpasien@gmail.com">hubpasien@gmail.com</a></p>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="single-contact">
                                        <i class="fas fa-map-marker-alt"></i>
                                        <div class="contact-text">
                                            <p>Tembau, Jl. Sangalangit, Penatih, Kec. Denpasar Tim., Kota Denpasar, Bali
                                                80238</p>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="footer-copyright text-center">
            <p>&copy; copyright <?php echo date('Y') ?> by <a href="#">hubpasien.com</a></p>
        </div>
    </div>
</footer>

<a href="#" class="back-to-top"><i class="fal fa-chevron-up"></i></a>

<script src="assets/js/plugins/hubpasien.min.js"></script>
<script async src="https://www.googletagmanager.com/gtag/js?id=G-YGCBCKV5ZM"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-YGCBCKV5ZM');
</script>

</body>

</html>
