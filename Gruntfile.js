module.exports = function (grunt) {

    var jsFiles = [
        'assets/js/vendor/jquery-3.5.1.min.js',
        'assets/js/vendor/modernizr-3.7.1.min.js',
        'assets/js/plugins/popper.min.js',
        'assets/js/plugins/bootstrap.min.js',
        'assets/js/plugins/swiper-bundle.min.js',
        'assets/js/plugins/jquery.easing.min.js',
        'assets/js/plugins/scrolling-nav.js',
        'assets/js/plugins/wow.min.js',
        'assets/js/main.js'
    ];
    var cssFiles = [
        'assets/css/plugins/bootstrap.min.css',
        'assets/css/plugins/fontawesome.min.css',
        'assets/css/plugins/flaticon.css',
        'assets/css/plugins/default.css',
        'assets/css/plugins/animate.css',
        'assets/css/plugins/swiper-bundle.min.css',
        'assets/css/style.css'
    ];
    var allFiles = jsFiles.concat(cssFiles); // merge js & css files directory

    grunt.initConfig({
        jsDistDir: 'assets/js/plugins/',
        cssDistDir: 'assets/css/plugins/',
        pkg: grunt.file.readJSON('package.json'),
        concat: {
            js: {
                options: {
                    separator: ';'
                },
                src: jsFiles,
                dest: '<%=jsDistDir%><%= pkg.name %>.js'
            },
            css: {
                src: cssFiles,
                dest: '<%=cssDistDir%><%= pkg.name %>.css'
            }
        },
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%=grunt.template.today("dd-mm-yyyy") %> */\n'
            },
            dist: {
                files: {
                    '<%=jsDistDir%><%= pkg.name %>.min.js': ['<%= concat.js.dest %>']
                }
            }
        },
        cssmin: {
            add_banner: {
                options: {
                    // banner: '/*! <%= pkg.name %> <%=grunt.template.today("dd-mm-yyyy") %> */\n'
                },
                files: {
                    '<%=cssDistDir%><%= pkg.name %>.min.css': ['<%= concat.css.dest %>']
                }
            }
        },
        watch: {
            files: allFiles,
            tasks: ['concat', 'uglify', 'cssmin']
        }
    });

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('default', [
        'concat',
        'uglify',
        'cssmin',
        'watch'
    ]);

};